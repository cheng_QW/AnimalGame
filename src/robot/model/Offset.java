package robot.model;

public class Offset {
	
	int x;
	int y;
	
	public Offset(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	
	public boolean isZero() {
		if(this.x == 0 && this.y == 0) {
			return true;
		}else {
			return false;
		}
	}
	@Override
	public String toString() {
		return "Coordinate [x=" + x + ", y=" + y + "]";
	}

}
