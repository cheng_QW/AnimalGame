package robot;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;


import robot.model.Coordinate;
import robot.model.Offset;
import robot.utils.ImgCmpUtil;

public class SimpleTest {
	
	static int cacheX;
	static int cacheY;
	
	final static String keyImagePath = "/images/key.png";
	final static String sjxcPath = "/images/sjxc1.png";
	final static String rightPath = "/images/sjxc.png";
	final static String left1Path = "/images/sjxc.png";
	final static String hua2Path = "/images/hua222.png";
	final static String xbPath = "/images/xb1.png";
	final static String xtPath = "/images/xt.png";
	final static String mssPath = "/images/mss11.png";
	final static String msswbPath = "/images/mss_wb1.png";
	final static String mss3Path = "/images/mss3.png";
	final static String mssblPath = "/images/mss_bl.png";
	final static String huabzPath = "/images/hua_bz1.png";
	
	final static String weixinPath = "/images/weixin.png";
	final static String wjzsPath = "/images/wjzs.png";
	
	final static String hdPath = "/images/exit/hd.png";
	final static String jyPath = "/images/exit/jy.png";
	final static String tgPath = "/images/exit/tg.png";
	
	//处理所有异常
	String[] filePaths = new String[] {huabzPath,hdPath,jyPath,tgPath};
	
    
    public static void main(String[] args) throws Exception {
    	final Robot rb = new Robot();
    	SimpleTest simple = new SimpleTest();
    	simple.clickIcon(rb, keyImagePath,null);
    	rb.delay(1000);
    	ImageFindDemo demo = new ImageFindDemo(sjxcPath);
        Coordinate coord = demo.printFindData();
        System.out.println(coord.toString());
		cacheX = coord.getX() - 222; 
		cacheY = coord.getY() - 468;
		System.out.println("cacheX:" + cacheX + ",cacheY:" + cacheY);
		//回到原位置
		simple.left(rb);
		simple.left(rb);
		simple.left(rb);
		simple.right(rb);
		simple.right(rb);
		simple.right(rb);
		simple.left(rb);
		simple.down(rb);
		simple.up(rb);
    	
    	int huaCount = 0;
    	
        int i = 0;
    	while (true) {
    		double random = Math.random() * 200;
    		rb.delay((int)Math.round(random));
    		simple.clickIcon(rb, sjxcPath,null);
    		i++;
    		if(i == 128) {
    			simple.food1(rb);
    			i = 0;
    			huaCount++;
    			simple.left(rb);
    			if(huaCount == 2) {
    				simple.fhua(rb);
    				huaCount = 0;
    			}
    			simple.left(rb);
    			simple.left(rb);
    			simple.right(rb);
    			simple.right(rb);
    			simple.right(rb);
    			simple.left(rb);
    			simple.down(rb);
    			simple.up(rb);
    		}
		}
    }
    	
    	//菜1
    	public void food1(Robot rb){
    		System.out.println("点菜1");
    		rb.delay(300);
			clickIcon(rb, sjxcPath,new Offset(163,316));
    	}
    	
		//向左
		public void left(Robot rb) {
			rb.delay(500);
			clickIcon(rb, sjxcPath,new Offset(266,244));
		}
		//向右
		public void right(Robot rb) {
			rb.delay(500);
			clickIcon(rb, sjxcPath,new Offset(-20, 235));
		}
		//向下
		public void down(Robot rb) {
			rb.delay(500);
			clickIcon(rb, sjxcPath,new Offset(163,-10));
		}
		//向上
		public void up(Robot rb) {
			rb.delay(500);
			clickIcon(rb, sjxcPath,new Offset(177,412));
		}
		
		
		//花园
		public void fhua(Robot rb) {
			hua(rb,0);
			hua(rb,1);
			//放花
			List<Offset> poffsets = Arrays.asList(new Offset(194 - 50,267),new Offset(70 - 50,267),new Offset(194 - 50 ,189),new Offset(70 - 50,189));
			for(Offset offset:poffsets) {
				rb.delay(500);
				clickIcon(rb, sjxcPath,offset);
				rb.delay(1500);
			}
		}
		//花
		private void hua(Robot rb,int flag) {
			//收花种花
	    	List<Offset> offsets = Arrays.asList(new Offset(194,267),new Offset(70 ,267),new Offset(194 ,189),new Offset(70 ,189));
			for(Offset offset:offsets) {
				rb.delay(500);
				clickIcon(rb, sjxcPath,offset);
				rb.delay(1500);
				if(flag == 1) {
					//播种偏移量
			    	clickIcon(rb, sjxcPath,new Offset(126,173));
				}
			}
		}
		
		
//		public void hua2(Robot rb,int offsetHuaY) {
//			rb.delay(500);
//			clickIcon(rb, sjxcPath,new Offset(70 - offsetHuaY,267));
//			if(offsetHuaY == 0) {
//				rb.delay(1500);
//			}
//		}
//		public void hua3(Robot rb,int offsetHuaY) {
//			rb.delay(500);
//			clickIcon(rb, sjxcPath,new Offset(194 - offsetHuaY,189));
//			rb.delay(1500);
//			if(offsetHuaY == 0) {
//				rb.delay(1500);
//			}
//		}
//		public void hua4(Robot rb,int offsetHuaY) {
//			rb.delay(500);
//			clickIcon(rb, sjxcPath,new Offset(70 - offsetHuaY,189));
//			if(offsetHuaY == 0) {
//				rb.delay(1500);
//			}
//			
//		}
    
    /**
     * @description 根据路径点击图标图标
     * @param rb
     */
    public void clickIcon(Robot rb,String keyImagePath,Offset offset) {
    	int offsetX = 0;
    	int offsetY = 0;
    	
    	ImageFindDemo demo = new ImageFindDemo(keyImagePath);
        Coordinate coord = demo.printFindData();
    	if(offset != null ) {
    		offsetX = offset.getX();
    		offsetY = offset.getY();
    		coord = demo.findCenterData();
    		System.out.println(coord.toString());
    	}
       
        if(isZero(rb, coord) > 5) {
        	System.out.println("退出！");
        	saveScreen(rb);
        	sendWeixin(rb);
        	System.exit(0);
        }else {
        	rb.mouseMove(coord.getX() - offsetX ,coord.getY() - offsetY);
            Common.clickLMouse(rb,coord.getX() - offsetX ,coord.getY() - offsetY, 200);
        }
    }
    
    int countExit = 0;
	public int isZero(Robot rb,Coordinate coord) {
		if(coord.getX() == 0 && coord.getY() == 0 
				&& !xb(rb) && !mss1(rb)) {
			
//			huabz(rb);
			findAndDo(rb, filePaths);
			Coordinate msswbPathCoord = new Coordinate(cacheX,cacheY);
			moveMouseClick(rb, msswbPathCoord);
			
			return countExit++;
		}else {
			return countExit = 0;
		}
	}
    
	public void moveMouseClick(Robot rb,Coordinate coord) {
		rb.mouseMove(coord.getX(),coord.getY());
        Common.clickLMouse(rb,coord.getX(),coord.getY(), 200);
	}
	
	//异常情况 - （提示休息）行吧 - （订单）好的
	public boolean xb(Robot rb) {
			ImageFindDemo demo = new ImageFindDemo(xbPath);
	        Coordinate coord = demo.printFindData();
	        if(coord.getX() != 0 && coord.getY() != 0) {
	        	System.out.println("休息会-行吧！");
	        	moveMouseClick(rb, coord);
	        	return true;
	        }else {
	        	return false;
	        }
	}
	//魔术师
	public boolean mss1(Robot rb) {
		ImageFindDemo demo = new ImageFindDemo(mssPath);
        Coordinate coord = demo.printFindData();
        if(coord.getX() != 0 && coord.getY() != 0) {
        	System.out.println("魔术师！");
        	moveMouseClick(rb, coord);
        	mss2(rb);
        	mss3(rb);
        	return true;
        }else {
        	Coordinate msswbPathCoord = new Coordinate(cacheX,cacheY);
			moveMouseClick(rb, msswbPathCoord);
        	return false;
        }
	}
	
	public void mss2(Robot rb) {
		rb.delay(1500);
		ImageFindDemo msswbDemo = new ImageFindDemo(msswbPath);
        Coordinate msswbPathCoord = msswbDemo.printFindData();
        if(msswbPathCoord.getX() != 0 && msswbPathCoord.getY() != 0) {
        	 System.out.println("魔术师-玩吧！");
             moveMouseClick(rb, msswbPathCoord);
        }else {
        	Coordinate catchCoord = new Coordinate(cacheX,cacheY);
			moveMouseClick(rb, catchCoord);
        }
       
	}
	
	public void mss3(Robot rb) {
		rb.delay(1500);
		ImageFindDemo msswbDemo = new ImageFindDemo(mss3Path);
        Coordinate msswbPathCoord = msswbDemo.printFindData();
        System.out.println("魔术师-翻牌！");
        moveMouseClick(rb, msswbPathCoord);
        msswbPathCoord.setX(cacheX);
        msswbPathCoord.setY(cacheY);
        rb.delay(2500);
        moveMouseClick(rb, msswbPathCoord);
        rb.delay(2500);
        mss4(rb);
	}
	
	public void mss4(Robot rb) {
		rb.delay(7000);
		ImageFindDemo msswbDemo = new ImageFindDemo(mssblPath);
        Coordinate msswbPathCoord = msswbDemo.printFindData();
        
        if(msswbPathCoord.getX() != 0 && msswbPathCoord.getY() != 0) {
        	moveMouseClick(rb, msswbPathCoord);
            System.out.println("魔术师-不了！");
       }else {
       		Coordinate catchCoord = new Coordinate(cacheX,cacheY);
			moveMouseClick(rb, catchCoord);
       }
	}
	
	public void huabz(Robot rb) {
		rb.delay(350);
		ImageFindDemo msswbDemo = new ImageFindDemo(huabzPath);
		Coordinate msswbPathCoord = msswbDemo.printFindData();
		moveMouseClick(rb, msswbPathCoord);
		System.out.println("种花！" + msswbPathCoord);
	}
	
	//截图-保存本地
	public void saveScreen(Robot rb)  {

		LocalDate localDate = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd HH：mm：ss");
		LocalDateTime localDateTime = localDate.atTime(LocalTime.now());
		String currentTime = localDateTime.format(formatter);
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int w = (int) screenSize.getWidth();
		int h = (int) screenSize.getHeight();
		BufferedImage screenImg = rb.createScreenCapture(new Rectangle(0, 0,
				w, h));
		try {
			OutputStream out1 = new FileOutputStream("D:/exit/screen" + currentTime + ".png");
			ImageIO.write(screenImg, "png", out1);//将截到的BufferedImage写到本地
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	//发送微信
	public void sendWeixin(Robot rb) {
		rb.delay(150);
		ImageFindDemo msswbDemo = new ImageFindDemo(weixinPath);
		Coordinate msswbPathCoord = msswbDemo.printFindData();
		moveMouseClick(rb, msswbPathCoord);
		rb.delay(250);
		ImageFindDemo wjzsDemo = new ImageFindDemo(wjzsPath);
		Coordinate wjzsCoord = wjzsDemo.printFindData();
		moveMouseClick(rb, wjzsCoord);
		rb.delay(250);
		
		
		int[] ks = { KeyEvent.VK_C, KeyEvent.VK_M,
                KeyEvent.VK_D, KeyEvent.VK_ENTER, };
		ks = new int[] { KeyEvent.VK_E, KeyEvent.VK_X, KeyEvent.VK_I,
                KeyEvent.VK_T, KeyEvent.VK_ENTER };
        Common.pressKeys(rb, ks, 500);
        rb.delay(1000);
        rb.keyPress(KeyEvent.VK_ENTER);
     	rb.keyRelease(KeyEvent.VK_ENTER);
	}
	
	
	//根据图片查找连续操作文件
	public void findAndDo(Robot rb,String[] paths) {
		int x = 0;
		int y = 0;
		for(String path : paths) {
			if(path.endsWith("_click")) {
				Coordinate coordinate = new ImgCmpUtil().isPatchExist(rb,path.split("_")[0]);
	        	if(coordinate == null) {
	        		continue;
	        	}
				x = coordinate.getX();
				y = coordinate.getY();
		        rb.mouseMove(x,y);
		        Common.clickLMouse(rb, x, y, 10);
		        Common.clickLMouse(rb, x,y, 500);
	        	
	        }else {
	        	Coordinate coordinate = new ImgCmpUtil().isPatchExist(rb,path);
	        	if(coordinate == null) {
	        		continue;
	        	}
				x = coordinate.getX();
				y = coordinate.getY();
		        rb.mouseMove(x,y);
		        Common.clickLMouse(rb, x,y, 800);
	        }
		}
	}
	
//	public void fx(Robot rb,String path) {
//		Coordinate coordinate = new ImgCmpUtil().isPatchExist(rb,path);
//		if(coordinate != null) {
//			Coordinate msswbPathCoord = new Coordinate(cacheX - 222,cacheY - 468);
//			moveMouseClick(rb, msswbPathCoord);
//		}
//	}
}