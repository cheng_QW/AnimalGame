package robot.fingerprint;

public class Client {
	public static void main(String[] args) {
	    
	    String filename = ImageHelper.path + "\\images\\";
	    String hashCode = SimilarImageSearch.produceFingerPrint(filename + "example1.jpg");
	    System.out.println("Resources: ");
	    System.out.println(hashCode);
	    
		String sourceHashCode = SimilarImageSearch.produceFingerPrint(filename + "source.jpg");
		System.out.println("Source: ");
		System.out.println(sourceHashCode);
		
		    int difference = SimilarImageSearch.hammingDistance(sourceHashCode, hashCode);
		    System.out.print("汉明距离:"+difference+"     ");
		    if(difference==0){
		    	System.out.println("source.jpg图片跟example1.jpg一样");
		    }else if(difference<=5){
		    	System.out.println("source.jpg图片跟example1.jpg非常相似");
		    }else if(difference<=10){
		    	System.out.println("source.jpg图片跟example1.jpg有点相似");
		    }else if(difference>10){
		    	System.out.println("source.jpg图片跟example1.jpg完全不一样");
		    }
	}
}